<?php

namespace App\Controller;

use App\Entity\Historia;
use App\Form\HistoriaType;
use App\Repository\HistoriaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/historia")
 */
class HistoriaController extends AbstractController
{
    /**
     * @Route("/", name="app_historia_index", methods={"GET"})
     */
    public function index(HistoriaRepository $historiaRepository): Response
    {
        return $this->render('historia/index.html.twig', [
            'historias' => $historiaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_historia_new", methods={"GET", "POST"})
     */
    public function new(Request $request, HistoriaRepository $historiaRepository): Response
    {
        $historium = new Historia();
        $form = $this->createForm(HistoriaType::class, $historium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $historiaRepository->add($historium);
            return $this->redirectToRoute('app_historia_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('historia/new.html.twig', [
            'historium' => $historium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_historia_show", methods={"GET"})
     */
    public function show(Historia $historium): Response
    {
        return $this->render('historia/show.html.twig', [
            'historium' => $historium,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_historia_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Historia $historium, HistoriaRepository $historiaRepository): Response
    {
        $form = $this->createForm(HistoriaType::class, $historium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $historiaRepository->add($historium);
            return $this->redirectToRoute('app_historia_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('historia/edit.html.twig', [
            'historium' => $historium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_historia_delete", methods={"POST"})
     */
    public function delete(Request $request, Historia $historium, HistoriaRepository $historiaRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$historium->getId(), $request->request->get('_token'))) {
            $historiaRepository->remove($historium);
        }

        return $this->redirectToRoute('app_historia_index', [], Response::HTTP_SEE_OTHER);
    }
}
